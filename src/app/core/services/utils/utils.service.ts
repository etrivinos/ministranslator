import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor(private alertController: AlertController) {}

  /**
   * Order an array of objects by object attribute
   * @param orderArrayByAttribute 
   * @param prop 
   */
  public orderArrayByAttribute(objs: any[], attribute: string, order: string = 'asc') {
    let orderNumber = order === 'asc' ? 1 : -1;
    let objsOrdered = objs.sort((a, b) => (a[attribute] > b[attribute]) ? 
      orderNumber : ((b[attribute] > a[attribute]) ? -orderNumber : 0));

    return objsOrdered;
  }

  /**
   * Convert object to URL query string
   * @method objectToQueryParams
   * @param params Object params
   * @return string Query string
   */
  public objectToQueryParams(params) {
    let queryArray = Object.keys(params).map((key) => {
      if(params[key] === undefined || params[key] === '' || params[key] === null) return '';
      return key + '=' + params[key];
    });

    let filterArray = queryArray.filter((item) => {
      if(item) return item;
    });

    if(filterArray.length) {
      return '?' + filterArray.join('&');
    }
    return '';
  }

  /**
   * Convert object of objects item to an array
   * @param objList
   */
  public convertObjectsToArray(objList: any) {
    let list = [];

    for(let key in objList) {
      let value = objList[key];
      list.push({'id': key, 'name': value });
    };

    return list;
  }

  /**
   * Show an error alert
   * @method showErrorAlert
   * @param {string} title Alert title
   * @param {string} description Alert subtitle
   */
  public async showErrorAlert(
    description: string
  ) {
    const alert = await this.alertController.create({
        header: 'Error',
        message: description,
        buttons: ['OK']
      });
  
    await alert.present();
  }
}
