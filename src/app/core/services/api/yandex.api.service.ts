import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UtilsService } from '../utils/utils.service';

@Injectable({
  providedIn: 'root'
})
export class YandexApiService {
  constructor(
    private http: HttpClient,
    private utilsService: UtilsService
  ) {}

  /**
   * Get the supported languages by the Yandax translation API
   * @method getSupportedLanguages
   * @param path Path string
   */
  public getSupportedLanguages(): Observable<any> {
    let url = environment.endpoint + 'getLangs';
    let queryParams = this.utilsService.objectToQueryParams({
      'key': environment.public_key,
      'ui': environment.default_language,
    });
    
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post(
      url + queryParams, 
      null, 
      { headers: headers }
    );
  }

  /**
   * Translate a text from one language to another language
   * @method translateText
   * @param {string} textToTranslate Text to translate
   * @param {string} fromLanguage From language
   * @param {string} toLanguage To language
   */
  public translateText(
    textToTranslate: string,
    fromLanguage: string, 
    toLanguage: string
  ): Observable<any> {
    let url = environment.endpoint + 'translate';

    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let queryParams = this.utilsService.objectToQueryParams({
      'lang': [fromLanguage, toLanguage].join('-'),
      'format': 'text',
      'key': environment.public_key,
      'text': textToTranslate
    });

    return this.http.post(
      url + queryParams, 
      null, 
      { headers: headers }
    );
  }
}
