import { Component, NgZone } from '@angular/core';
import { YandexApiService } from '../core/services/api/yandex.api.service';
import { AlertController, LoadingController } from '@ionic/angular';
import { UtilsService } from '../core/services/utils/utils.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public supportedLanguages = [];

  public startLanguage: string = '';
  public textToTranslate: string = '';

  public endLanguage: string = '';
  public textTranslated: string = '';

  constructor(
    private yandexService: YandexApiService,
    private utils: UtilsService,
    private loadingController: LoadingController
  ) {
    this.getSupportedLanguages();
  }

  /**
   * Get the supported languages
   * @method getSupportedLanguages
   */
  private async getSupportedLanguages() {
    const loading = await this.loadingController.create({ 
      message: 'Getting the supported languages...' 
    });
    await loading.present();

    this.yandexService.getSupportedLanguages()
      .toPromise()
      .then((supportedLanguages: any) => {
        let languages = this.utils.convertObjectsToArray(supportedLanguages.langs);
        this.supportedLanguages = this.utils.orderArrayByAttribute(languages, 'name');

        loading.dismiss();
      })
      .catch((e) => {
        this.utils.showErrorAlert(e.message);
        loading.dismiss();
      });
  }

  /**
   * Translate the text
   * @method translateText
   */
  public async translateText() {
    if(!this.textToTranslate || !this.startLanguage || !this.endLanguage) {
      this.utils.showErrorAlert(
        'You must enter a valid text to translate and select the languages.'
      );
      return;
    }

    const loading = await this.loadingController.create({ 
      message: 'Translating...' 
    });
    await loading.present();

    this.yandexService.translateText(
      this.textToTranslate,
      this.startLanguage,
      this.endLanguage
    )
      .toPromise()
      .then((response) => {
        this.textTranslated = response.text[0];
        loading.dismiss();
      })
      .catch((e) => {
        loading.dismiss();
        this.utils.showErrorAlert(e.message);
      });
  }

}
