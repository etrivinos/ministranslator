export const environment = {
  production: true,
  endpoint: 'https://translate.yandex.net/api/v1.5/tr.json/',
  public_key: 'trnsl.1.1.20200205T233005Z.5e79d21d43eacfa9.ecbd7346e852b59ea0cda1c23c191dd957b75ae4',
  default_language: 'en'
};
